package com.phraenck.gongfutimer;

public interface ITimer {
    void onTick(long ms);
    void onFinish();
    void onCooldownFinish();
    void refillReminder();
}
