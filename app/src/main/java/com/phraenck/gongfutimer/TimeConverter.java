package com.phraenck.gongfutimer;

import org.jetbrains.annotations.NotNull;

public class TimeConverter {

    /**
     * getTimeStrFromMilli
     * Convert milliseconds to minutes:seconds
     * @param milliseconds Milliseconds
     * @return minutes:seconds
     */
    @NotNull
    public static String getTimeStrFromMilli(long milliseconds) {
        int minutes = (int) (milliseconds / 60000);
        int seconds = (int) (milliseconds / 1000) % 60;

        return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
    }

    /**
     * Parse an mm:ss formatted string into milliseconds
     * @param timestr mm:ss formatted string representing minutes and seconds
     * @return the number of milliseconds timestr represents
     * @throws Exception thrown if timestr is not formatted correctly
     */
    public static int getMsFromTimeStr(String timestr) throws Exception {
        String[] tokens = timestr.split(":");

        if (tokens.length == 2) {
            int minutes = Integer.parseInt(tokens[0].trim());
            int seconds = Integer.parseInt(tokens[1].trim());

            //convert to milliseconds
            return ((60 * minutes) + seconds) * 1000;
        } else {
            throw new Exception("Invalid time");
        }
    }
}
